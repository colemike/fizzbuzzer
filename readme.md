Building FizzBuzzer
-------------------

**Requirement**

Visual Studio 2015 or [.NET Framework 4.5.2 Developer Pack](https://download.microsoft.com/download/B/4/1/B4119C11-0423-477B-80EE-7A474314B347/NDP452-KB2901954-Web.exe).

**To Build**

Run *psake.bat*.