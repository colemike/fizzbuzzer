Framework '4.5.1x86'

properties {
	$base_dir = resolve-path .
	$source_dir = "$base_dir\src"
    $tools_dir = "$base_dir\tools"
	$global:config = "debug"
}

task default -depends clean, compile, test

task clean {
	rd "$source_dir\artifacts" -recurse -force  -ErrorAction SilentlyContinue | out-null
	rd "$base_dir\build" -recurse -force  -ErrorAction SilentlyContinue | out-null
}

task compile -depends clean {
    exec { & $tools_dir\Nuget.exe restore $source_dir\FizzBuzzer.sln }

    exec { msbuild /t:Clean /t:Build /p:Configuration=$config /v:q /p:NoWarn=1591 /nologo $source_dir\FizzBuzzer.sln }
}

task test {
    $testRunners = @(gci $source_dir\packages -rec -filter xunit.console.exe)

    if ($testRunners.Length -ne 1)
    {
        throw "Expected to find 1 xunit.console.exe, but found $($testRunners.Length)."
    }

    $testRunner = $testRunners[0].FullName

    exec { & $testRunner $source_dir/FizzBuzzer.Tests/bin/$config/FizzBuzzer.Tests.dll }
}