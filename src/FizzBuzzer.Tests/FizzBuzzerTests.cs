﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Shouldly;
using Xunit;

namespace FizzBuzzer.Tests
{
    public class FizzBuzzerTests
    {
        [Fact]
        public void Run_ThrowsArgumentNullException_OnNullCombinationsParameter()
        {
            //arrange
            var fizzBuzzer = new FizzBuzzer(new FakeOutput());

            //assert
            var thrownException = Assert.Throws<ArgumentNullException>(() => fizzBuzzer.Run(null, 100));
            thrownException.ParamName.ShouldBe("combinations");
        }

        [Fact]
        public void Run_ThrowsArgumentOutOfRangeException_OnNegativeUpperBoundParameter()
        {
            //arrange
            var fizzBuzzer = new FizzBuzzer(new FakeOutput());
            var combinations = new List<FizzBuzzCombination>();

            //assert
            var thrownException = Assert.Throws<ArgumentOutOfRangeException>(() => fizzBuzzer.Run(combinations, -1));
            thrownException.ParamName.ShouldBe("upperBound");
        }

        [Fact]
        public void Run_StopsAtTen_WhenUpperBoundIsTen()
        {
            //arrange
            var fakeOutput = new FakeOutput();
            var fizzBuzzer = new FizzBuzzer(fakeOutput);
            var combinations = new List<FizzBuzzCombination>();

            //act
            fizzBuzzer.Run(combinations, 10);

            //Assert
            fakeOutput.Results.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None).Length.ShouldBe(10);
        }

        [Fact]
        public void Run_CatchesCombination_WhenSingleCombinationAndSingleInstance()
        {
            //arrange
            var fakeOutput = new FakeOutput();
            var fizzBuzzer = new FizzBuzzer(fakeOutput);
            var combinations = new List<FizzBuzzCombination>
            {
                new FizzBuzzCombination { Index=5, Value="Foo" }
            };

            //act
            fizzBuzzer.Run(combinations, 5);

            //Assert
            fakeOutput.Results.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None).Last().ShouldBe("Foo");
        }

        [Fact]
        public void Run_CatchesCombination_WhenSingleCombinationAndMultipleInstance()
        {
            //arrange
            var fakeOutput = new FakeOutput();
            var fizzBuzzer = new FizzBuzzer(fakeOutput);
            var expected = "Foo";
            var combinations = new List<FizzBuzzCombination>
            {
                new FizzBuzzCombination { Index=5, Value=expected }
            };

            //act
            fizzBuzzer.Run(combinations, 10);

            //Assert
            var output = fakeOutput.Results.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            output.ElementAt(4).ShouldBe(expected);
            output.ElementAt(9).ShouldBe(expected);
        }

        [Fact]
        public void Run_CatchesCombination_WhenMultipleCombinationAndSingleInstance()
        {
            //arrange
            var fakeOutput = new FakeOutput();
            var fizzBuzzer = new FizzBuzzer(fakeOutput);
            var combinations = new List<FizzBuzzCombination>
            {
                new FizzBuzzCombination { Index=4, Value="Foo" },
                new FizzBuzzCombination { Index=5, Value="Bar" }
            };

            //act
            fizzBuzzer.Run(combinations, 5);

            //Assert
            var output = fakeOutput.Results.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            output.ElementAt(3).ShouldBe("Foo");
            output.ElementAt(4).ShouldBe("Bar");
        }

        [Fact]
        public void Run_CatchesCombination_WhenMultipleCombinationAndMultipleInstance()
        {
            //arrange
            var fakeOutput = new FakeOutput();
            var fizzBuzzer = new FizzBuzzer(fakeOutput);
            var expectedFor4 = "Foo";
            var expectedFor5 = "Bar";
            var combinations = new List<FizzBuzzCombination>
            {
                new FizzBuzzCombination { Index=4, Value=expectedFor4 },
                new FizzBuzzCombination { Index=5, Value=expectedFor5 }
            };

            //act
            fizzBuzzer.Run(combinations, 10);

            //Assert
            var output = fakeOutput.Results.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None);
            output.ElementAt(3).ShouldBe(expectedFor4);
            output.ElementAt(4).ShouldBe(expectedFor5);
            output.ElementAt(7).ShouldBe(expectedFor4);
            output.ElementAt(9).ShouldBe(expectedFor5);
        }

        [Fact]
        public void Run_CatchesCombination_WhenCollidingCombination()
        {
            //arrange
            var fakeOutput = new FakeOutput();
            var fizzBuzzer = new FizzBuzzer(fakeOutput);
            var combinations = new List<FizzBuzzCombination>
            {
                new FizzBuzzCombination { Index=4, Value="Foo" },
                new FizzBuzzCombination { Index=5, Value="Bar" }
            };

            //act
            fizzBuzzer.Run(combinations, 20);

            //Assert
            fakeOutput.Results.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None).Last().ShouldBe("Foo Bar");
        }

        [Fact]
        public void Run_CatchesCombinations_WhenMulitpleCombinationForSameNumber()
        {
            //arrange
            var fakeOutput = new FakeOutput();
            var fizzBuzzer = new FizzBuzzer(fakeOutput);
            var combinations = new List<FizzBuzzCombination>
            {
                new FizzBuzzCombination { Index=5, Value="Foo" },
                new FizzBuzzCombination { Index=5, Value="Bar" }
            };

            //act
            fizzBuzzer.Run(combinations, 5);

            //Assert
            fakeOutput.Results.ToString().Split(new[] { Environment.NewLine }, StringSplitOptions.None).Last().ShouldBe("Foo Bar");
        }
    }

    public class FakeOutput : IOutput
    {
        public void Output(string value)
        {
            Results.Append(value);
        }

        public void Output(int value)
        {
            Results.Append(value);
        }

        public StringBuilder Results { get; set; } = new StringBuilder();
    }
}