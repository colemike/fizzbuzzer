﻿namespace FizzBuzzer
{
    public interface IOutput
    {
        void Output(string value);
        void Output(int value);
    }
}