﻿namespace FizzBuzzer
{
    public class FizzBuzzCombination
    {
        public int Index { get; set; }

        public string Value { get; set; }
    }
}