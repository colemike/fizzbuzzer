﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace FizzBuzzer
{
    public class FizzBuzzer
    {
        private readonly IOutput _output;

        public FizzBuzzer(IOutput output)
        {
            _output = output;
        }

        public void Run(List<FizzBuzzCombination> combinations, int upperBound)
        {
            if (combinations == null)
            {
                throw new ArgumentNullException(nameof(combinations));
            }

            if (upperBound < 0)
            {
                throw new ArgumentOutOfRangeException(nameof(upperBound));
            }

            Func<int, int, bool> isMatch = (index, comb) => index % comb == 0;
            for (var index = 1; index <= upperBound; index++)
            {
                var result = new StringBuilder();
                var matchingCombs = combinations.Where(c => isMatch(index, c.Index)).ToList();

                if (matchingCombs.Any())
                {
                    result.Append(string.Join(" ", matchingCombs.Select(c => c.Value)));
                }
                else
                {
                    result.Append(index);
                }

                if (index != upperBound)
                {
                    result.AppendLine(string.Empty);
                }

                _output.Output(result.ToString());
            }
        }
    }
}