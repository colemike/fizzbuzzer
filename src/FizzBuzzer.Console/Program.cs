﻿using System.Collections.Generic;

namespace FizzBuzzer.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            var combinations = new List<FizzBuzzCombination>
            {
                new FizzBuzzCombination {Index = 3, Value = "Fizz"},
                new FizzBuzzCombination {Index = 3, Value = "Boo"},
                new FizzBuzzCombination {Index = 5, Value = "Buzz"}
            };

            new FizzBuzzer(new ConsoleOutput()).Run(combinations, 21);
            System.Console.ReadKey();
        }
    }
}
