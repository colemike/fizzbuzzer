﻿namespace FizzBuzzer.Console
{
    public class ConsoleOutput : IOutput
    {
        public void Output(string value)
        {
            System.Console.Write(value);
        }

        public void Output(int value)
        {
            System.Console.Write(value);
        }
    }
}